""" This module receives a word and returns its Pig Latin form."""
def main():
    while True:
        word = input("Introduce the word to traduce to Pig Latin:")

        first_letter = word[0:1]

        if first_letter in "aeiou":
            final_word = word + "way"
        else:
            final_word = word[1:] + first_letter + "ay"

        print(final_word)

        again = input("Again? y/n")

        if again == "y":
            pass
        else:
            break


if __name__ == '__main__':
    main()
