import urllib.request as request
import json


def main():
    while True:
        text = input("Introduce the text you want to analyze:")

        selection = input("Do you wish to translate the text? Y/N").lower()

        if selection == "y":
            language = input("Introduce the language code to translate the text:")
            translated_text = translate(text, language)
            frequencies = count_letters(str(translated_text))
            draw_frequencies_chart(frequencies)
        else:
            frequencies = count_letters(text)
            draw_frequencies_chart(frequencies)

        again = input("Again? Y/N")

        if again == "N":
            break

def detect_language(text):
    text = text.replace(" ", "%20")

    url = "https://translate.yandex.net/api/v1.5/tr.json/detect" + \
          "?key=trnsl.1.1.20190730T021002Z.4dbd890bd5f73432.6deb4b85c4a374014009dd5e2e38da2f50f7066b" + \
          "&text=" + text

    json_response = request.urlopen(url)
    json_object = json.load(json_response)
    print("Detected language: " + json_object["lang"])

    return str(json_object["lang"])


def translate(text, to_language_code):
    text = text.replace(" ", "%20")

    url = "https://translate.yandex.net/api/v1.5/tr.json/translate" +\
          "?key=trnsl.1.1.20190730T021002Z.4dbd890bd5f73432.6deb4b85c4a374014009dd5e2e38da2f50f7066b" + \
          "&text=" + text + "&lang="+ to_language_code

    json_response = request.urlopen(url)
    json_object = json.load(json_response)
    print(json_object["text"])

    return json_object["text"]

def count_letters(text):
    text = text.lower()
    frequencies = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0}
    for letter in text:
        if letter in "aeiou":
            old_freq = frequencies.get(letter)
            frequencies.update({letter : old_freq + 1})
    return frequencies

def draw_frequencies_chart(frequencies):
    letters = frequencies.keys()
    for letter in letters:
        letter_bar = "{}".format(letter)*frequencies.get(letter)
        print(letter + ":" + letter_bar)


if __name__ == '__main__':
    main()
    #translate("Hola me gusta el pene", "en")
